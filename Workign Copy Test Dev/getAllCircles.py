import cv2
import numpy as np
import win32api, win32con


filename = '5.jpg'
image = cv2.imread(filename)


# get first 2 mouse clicks
firstClickDone = False
secondClickDone = False
def getMouseClick(event,x,y,flags,param):
    global mouseX1,mouseY1,mouseX2,mouseY2
    global secondClickDone, firstClickDone
    
    global gx,gy
    gx,gy = x,y
    
    if event == cv2.EVENT_LBUTTONDOWN:
        if not firstClickDone:
            mouseX1,mouseY1 = x,y
            firstClickDone = True
            print(mouseX1,mouseY1)
        else:
            mouseX2,mouseY2 = x,y
            print(mouseX2,mouseY2)
            secondClickDone = True

cv2.namedWindow('inputpoints')
cv2.setMouseCallback('inputpoints',getMouseClick)
cv2.imshow('inputpoints',image)

while(1):
    k = cv2.waitKey(1) & 0xFF

    if firstClickDone and secondClickDone:
        break

    if k == 13:
        if not firstClickDone:
            mouseX1,mouseY1 = gx,gy
            print(mouseX1,mouseY1)
            firstClickDone = True
        else:
            mouseX2,mouseY2 = gx,gy
            print(mouseX2,mouseY2)
            secondClickDone = True
    elif k==97: #left
        gx = gx-1
        a,b = win32api.GetCursorPos()
        win32api.SetCursorPos((a-1,b))
    elif k==119: #up
        gy=gy-1
        a,b = win32api.GetCursorPos()
        win32api.SetCursorPos((a,b-1))
    elif k==100: #right
        gx=gx+1
        a,b = win32api.GetCursorPos()
        win32api.SetCursorPos((a+1,b))
    elif k==115: #down
        gy=gy+1
        a,b = win32api.GetCursorPos()
        win32api.SetCursorPos((a,b+1))


cv2.destroyAllWindows()
