from ClockTests import *
import os
import pickle

class CalibData:

    def __init__(self, numberofclocks=0):
        self.numberOfClocks = numberofclocks
        self.calibData = [] # this will be in format [{'12': ,'3': , '6': , '9':},]

    def insertCalib(self,twelve, three ,six, nine):
        dict_of_data = { '12':twelve, '3':three, '6':six, '9':nine}
        self.calibData.append(dict_of_data)

    def setNumberOfClocks(self,num): #setting this will also clear thecalibData
        self.numberOfClocks = num
        self.calibData = []
    
    def performSanityCheck(self):
        if (self.numberOfClocks <= 0):
            return False
        
        totalCalibData = len(self.calibData )
        if ( totalCalibData != self.numberOfClocks): #this means that some calib data is missing
            return False

        #check if all points are present for each of the clock
        points = ['12','3','6','9']
        for clock in self.calibData:
            for point in points:
                if point in clock:
                    data = clock[point]
                    try:
                        x = int(data[0])
                        y = int(data[1])
                    except:
                        return False
                else:
                    return False
        return True


    def saveCalibData(self):
        filename = "settings/calib.dat"

        try:
            output_file = open(filename,"wb")
            pickle.dump(self, output_file, pickle.HIGHEST_PROTOCOL)
        except:
            return( (FAIL, "Error while saving Calib Data."  ) )

        return( (PASS, "Calib Data Saved." ) )
        
    def loadCalibData(self):
        filename = "settings/calib.dat"
        
        if not os.path.isfile(filename):
            return( (CALIB_DATA_NOT_FOUND, "Calibration Data Not found. Re Calibrate Camera" ) )
        
        try:
            self.__dict__ = pickle.load( open( filename, "rb" ) ).__dict__
        except:
            return( (FAIL, "Error While Loading Calibration Data" ) )

        #perform a sanity check of the data before passign it off as valid data.
        # will just check if the number of data is as expected or not
        
        if (self.performSanityCheck()):
            return( (PASS, "Data successfully Loaded" ) )
        else:
            self.numberOfClocks = 0
            self.calibData = [] # this will be in format [{'12': ,'3': , '6': , '9':},]
            return( (FAIL, "Loaded Calibration data is invalid" ) )


        

        
    
