import cv2
import numpy as np
import math

class TimeTeller():

    def __init__(self, image = None, calibData=None):

        #self.image = None
        #self.time = {'HH':12,'MM':00,'SS':00}
        #self.calibdata = {'12':0,'2':0,'3':0,'9':0}

        self.image = image.copy() #creating copy so that original is not altered
        self.time = None
        self.calibData = calibData


    def transformImage(self):
        kernel = np.ones((5,5),np.uint8)
        self.image = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)
        ret,self.image = cv2.threshold(self.image,127,255,cv2.THRESH_BINARY)
        #self.image = cv2.morphologyEx(self.image, cv2.MORPH_CLOSE, kernel)
        cv2.imshow("output",  self.image)

    def iterateThroughRadius(self,r,centerx, centery):
        angle = 0.005
        startingblack = False
        xpoints = []
        ypoints = []
        blackInProgress = False
        hands = []
        thisone = []
        
        while (angle <= 2*3.14):
            pointy = int(r*np.cos(angle))
            pointx = int(r*np.sin(angle))
            if(self.image[centery+pointy][centerx+pointx] == 0):
                #self.image[centery+pointy][centerx+pointx] = (0, 255, 0)
                thisone.append((centerx+pointx,centery+pointy))
                blackInProgress = True
            else:
                if blackInProgress:
                    hands.append(thisone)
                    thisone = []
                blackInProgress = False
            angle+=0.00005

        
        for hand in hands:
            sumx = 0
            sumy = 0
            for x,y in hand:
                sumx += x
                sumy += y
            #self.image[int(sumy/len(hand))][int(sumx/len(hand))] = (255, 0, 0)
        return hands
        
    
    def readTime(self, image = None):
        if image != None:
            self.image = image.copy() #making an image coz we need to edit it
        
        if (self.image == None):
            return False

        self.transformImage()
        
        centerx = int((self.calibData['12'][0] + self.calibData['6'][0])/2)
        centery = int((self.calibData['3'][1] + self.calibData['9'][1])/2)
        radius = int(math.sqrt( (centerx - self.calibData['12'][0])**2 + (centery - self.calibData['12'][1])**2 ) )

        hands_set1 = self.iterateThroughRadius(radius-170, centerx, centery)
        hands_set2 = self.iterateThroughRadius(radius-195, centerx, centery)
        hands_set3 = self.iterateThroughRadius(radius-270, centerx, centery)

        
        refAngle = np.arctan2(self.calibData['12'][1] - self.calibData['6'][1],self.calibData['12'][0] - self.calibData['6'][0])
        refAngle = (refAngle if refAngle>0 else 2*np.pi+refAngle)*360/(2*np.pi)

        
        

        
        

