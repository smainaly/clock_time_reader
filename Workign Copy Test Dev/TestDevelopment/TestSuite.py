from ClockTests import *
from TestSequence import *
import os.path
import pickle

class TestSuite:

    def __init__(self, name="", developer = "", description = ""):
        self.suite_name = name
        self.suite_developer = developer
        self.suite_description = description
        self.suite_sequence = []

    def add_to_suite(self,test_sequence):
        self.suite_sequence.append(test_sequence)

    
    def move_item_up(self,index):
        if(index == 0):
            return((FAIL, "Already First Item."))
        if( index < 0 or index > len(self.suite_sequence)-1):
            return((FAIL, "Invalid item in list."))
        self.suite_sequence.insert(index-1,self.suite_sequence.pop(index))
        return( (PASS, "No Errors" ) )     
        

    def move_item_down(self,index):
        if(index == len(self.suite_sequence)-1):
            return((FAIL, "Already Last Item."))
        if( index < 0 or index > len(self.suite_sequence)-1):
            return((FAIL, "Invalid item in list."))
        self.suite_sequence.insert(index+1,self.suite_sequence.pop(index))
        return( (PASS, "No Errors" ) )


    def delete_item(self,index):
        if( index < 0 or index > len(self.suite_sequence)-1):
            return((FAIL, "Invalid item in list."))
        self.suite_sequence.pop(index)
        return( (PASS, "No Errors" ) )

    def save(self, over_ride = False):
        if self.suite_name == None or len(self.suite_name) == 0:
            return( (FAIL, "Test Suite Name Missing" ) )

        filename = "tests/suites/"+self.suite_name+".sui"
        if not over_ride and os.path.isfile(filename):
            return( (ALREADY_EXISTS, "Suite Name already exists. It has to be Unique." ) )

        if len(self.suite_sequence) == 0:
            return( (FAIL, "Suite has 0 sequences. Add some sequences." ) )
        
        try:
            output_file = open(filename,"wb")
            pickle.dump(self, output_file, pickle.HIGHEST_PROTOCOL)
        except:
            return( (FAIL, "Error while saving Suite."  ) )
        
        return( (PASS, "Test Suite Saved." ) )

    def load(self,name=None):
        self.suite_name = name if (name is not None) else self.suite_name
        if self.suite_name == None or self.suite_name == "":
            return( (FAIL, "No Suote Name Given" ) )

        filename = "tests/suites/"+self.suite_name+".sui"
        if not os.path.isfile(filename):
            return( (FAIL, "Suite with that name does not exist" ) )

        try:
            self.__dict__ = pickle.load( open( filename, "rb" ) ).__dict__
        except:
            return( (FAIL, "Error While Loading Suite" ) )

        
        return( (PASS, "Suite Loaded Successfully" ) )
        

