import sys
from PyQt5.QtWidgets import QMainWindow, QTextEdit, QAction, QApplication
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QHBoxLayout, QGroupBox, QDialog, QVBoxLayout, QGridLayout, QToolButton, QStyleOptionToolButton

from CreateNewTestSuite import TestSuiteCreator
from CreateNewTestSeq import TestSeqCreator
from CalibCamera import CameraCalib

class ATSTestSystem(QMainWindow):
    
    def __init__(self):
        super().__init__()
        self.title = 'ATS Testing System'
        self.left = 100
        self.top = 100
        self.width = 500
        self.height = 300
        self.initUI()
        
        
    def initUI(self):               

        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        
        
        exitAction = QAction(QIcon('exit24.png'), 'Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(self.close)

        self.statusBar()
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(exitAction)

        toolbar = self.addToolBar('Exit')
        toolbar.addAction(exitAction)

        buttonGroup = self.createButtons()
        self.setCentralWidget(buttonGroup)
        
        self.statusBar().showMessage("Ready!")
        self.show()

    def createButtons(self):
        horizontalGroupBox = QGroupBox()
        layout = QGridLayout()
        
        calib_cam_butt = QPushButton()
        calib_cam_butt.setText("Calibrate Camera")
        calib_cam_butt.setFixedSize(100, 100)
        calib_cam_butt.released.connect(self.calib_camera)
        
        
        start_test_butt = QPushButton()
        start_test_butt.setText("Start Testing")
        start_test_butt.setFixedSize(100, 100)
        start_test_butt.released.connect(self.start_testing)
        
        create_test_suite_butt = QPushButton()
        create_test_suite_butt.setText("Create/Edit/Delete \n Test Suite")
        create_test_suite_butt.setFixedSize(100, 100)
        create_test_suite_butt.released.connect(self.crud_test_suite)
        
        create_test_seq_butt = QPushButton()
        create_test_seq_butt.setText("Create/Edit/Delete \n Test Sequence")
        create_test_seq_butt.setFixedSize(100, 100)
        create_test_seq_butt.released.connect(self.crud_test_seq)

        layout.addWidget(calib_cam_butt,0,0) 
        layout.addWidget(start_test_butt,0,1) 
        layout.addWidget(create_test_suite_butt,1,0) 
        layout.addWidget(create_test_seq_butt,1,1) 
 
        horizontalGroupBox.setLayout(layout)

        return horizontalGroupBox

    def calib_camera(self):
        
        a = CameraCalib(self)
        a.show()
        return
    
    def crud_test_seq(self):
        a = TestSeqCreator(self)
        a.show()
        return
    
    def start_testing(self):
        print("Start Testing")
        return

    def crud_test_suite(self):
        a = TestSuiteCreator(self)
        a.show()
        return
        
if __name__ == '__main__':
    
    app = QApplication(sys.argv)
    ex = ATSTestSystem()
    sys.exit(app.exec_())
