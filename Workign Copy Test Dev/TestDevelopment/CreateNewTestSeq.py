import sys
from PyQt5.QtWidgets import QMainWindow, QTextEdit, QAction, QApplication
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QScrollArea, QApplication, QWidget, QPushButton, QHBoxLayout, QGroupBox, QDialog, QVBoxLayout, QGridLayout, QToolButton, QStyleOptionToolButton
from PyQt5.QtWidgets import QLabel, QLineEdit, QListWidget, QListWidgetItem, QTableWidget, QTableWidgetItem, QAbstractItemView
import easygui
from ClockTests import *
from TestSequence import *
import os

class TestSeqCreator(QMainWindow):
    
    def __init__(self, parent=None):
        super().__init__(parent)
        self.parent = parent
        self.title = 'Add new Test Sequence'
        self.left = 100
        self.top = 100
        self.width = 750
        self.height = 700
        self.initUI()
        self.testSeq = TestSequence()
        
    def initUI(self):               
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        
        exitAction = QAction(QIcon('exit24.png'), 'Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(self.close)

        self.statusBar()
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(exitAction)

        toolbar = self.addToolBar('Exit')
        toolbar.addAction(exitAction)

        topInputGroup = self.createTopInputs()
        functionGroup = self.createFunctionList()
        outputPanel = self.createOutputPanel()
        buttonPanel = self.createButtonPanel()
        allFilePanel = self.createAllFilePanel()
        allfilebuttonPanel = self.createAllFileButtonPanel()
        
        mainGroup = QGroupBox()
        mainLayout = QGridLayout()

        mainLayout.addWidget(allFilePanel, 0,0,2,1)
        mainLayout.addWidget(allfilebuttonPanel,2,0)
        mainLayout.addWidget(topInputGroup,0,1)
        mainLayout.addWidget(functionGroup,1,1)
        mainLayout.addWidget(outputPanel,1,2)
        mainLayout.addWidget(buttonPanel, 2,2)
        
        
        mainGroup.setLayout(mainLayout)
        
        self.setCentralWidget(mainGroup)
        
        self.statusBar().showMessage("Ready!")

        self.loadAllFiles()
        self.updateAllFileUI()
        
        self.show()

    def createTopInputs(self):
        topInputsBox = QGroupBox()
        topInputsBox.setMaximumWidth(300)
        topInputsBox.setMinimumWidth(300)
        layout = QGridLayout()

        nameLabel = QLabel("Name:")
        self.nameInput = QLineEdit()
        developerLabel = QLabel("Developer:")
        self.developerInput = QLineEdit()
        descriptionLabel = QLabel("Description:")
        self.descriptionInput = QLineEdit()
        #descriptionInput = QTextEdit()
        #descriptionInput.setFixedHeight(40)
        
        layout.addWidget(nameLabel, 0, 0)
        layout.addWidget(self.nameInput, 0, 1)
        layout.addWidget(developerLabel, 1, 0)
        layout.addWidget(self.developerInput, 1, 1)
        layout.addWidget(descriptionLabel, 2, 0)
        layout.addWidget(self.descriptionInput, 2, 1)
        topInputsBox.setLayout(layout)
        return topInputsBox

    def createFunctionList(self):
        functionList = QGroupBox()
        functionList.setMaximumWidth(300)
        functionList.setMinimumWidth(300)
        layout = QGridLayout()

        for test_name in listOfTests:
           button = QPushButton(test_name)
           button.setFixedSize(170,25)
           button.released.connect(self.addNewFunctionToSeq)
           layout.addWidget(button)

        functionList.setLayout(layout)
        return functionList

    def createButtonPanel(self):
        buttons = QGroupBox()
        buttons.setMaximumWidth(350)
        layout = QGridLayout()
        
        save_button = QPushButton("Save")
        save_button.released.connect(self.saveClicked)

        new_button = QPushButton("Start New Sequence")
        new_button.released.connect(self.startNew)
        
        exit_button = QPushButton("Exit")
        exit_button.released.connect(self.close)
        layout.addWidget(save_button,0,0)
        layout.addWidget(new_button,0,1)
        layout.addWidget(exit_button,0,2)
        
        
        buttons.setLayout(layout)
        return buttons

    def createAllFileButtonPanel(self):
        buttons = QGroupBox()
        buttons.setMaximumWidth(150)
        layout = QGridLayout()

        refresh_button = QPushButton("Refresh List")
        refresh_button.released.connect(self.refreshAllFiles)
        layout.addWidget(refresh_button,0,0)
        buttons.setLayout(layout)

        return buttons
        
        
        
    def createOutputPanel(self):
        self.outputList = QTableWidget(0,4)
        self.outputList.verticalHeader().setVisible(False)
        self.outputList.horizontalHeader().setVisible(False)
        self.outputList.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.outputList.setColumnWidth(0,250)
        self.outputList.setColumnWidth(1,20)
        self.outputList.setColumnWidth(2,20)
        self.outputList.setColumnWidth(3,20)
        self.outputList.itemClicked.connect(self.cellClicked)
        self.outputList.setShowGrid(False)
        
        outputBoxLayout = QVBoxLayout()
        self.outputList.setMinimumWidth(350)
        self.outputList.setMaximumWidth(350)

        self.outputList.setLayout(outputBoxLayout)
        return self.outputList


    def createAllFilePanel(self):
        self.allFileList = QTableWidget(0,2)
        self.allFileList.verticalHeader().setVisible(False)
        self.allFileList.horizontalHeader().setVisible(False)

        self.allFileList.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.allFileList.setColumnWidth(0,200)
        self.allFileList.setColumnWidth(1,20)
        self.allFileList.itemClicked.connect(self.allFileClicked)

        self.allFileList.itemDoubleClicked.connect(self.allFileDoubleClicked)

        self.allFileList.setShowGrid(False)
        
        layout = QVBoxLayout()
        self.allFileList.setMinimumWidth(230)
        self.allFileList.setLayout(layout)
        return self.allFileList
        
        
    def addNewFunctionToSeq(self):
        functionPressed = self.sender().text()
        #get the required parameters
        msg = "Additional Information required for " + functionPressed
        title = functionPressed + " Parameters"
        fieldNames = Tests[functionPressed][1]
        fieldValues = []
        #only show input field if there are parameters to enter
        if(len(fieldNames) > 0 ):
            fieldValues = easygui.multenterbox(msg,title, fieldNames)
            while True:
                if(fieldValues == None):
                    break
                errmsg = ""
                for i in range(len(fieldNames)):
                    if fieldValues[i].strip() == "":
                        errmsg = errmsg+"\nPlease Enter "+fieldNames[i]
                if errmsg == "":
                    break #this means all inputs have been entered
                fieldValues = easygui.multenterbox(errmsg, title, fieldNames, fieldValues)

        #testSeq.add_to_sequence(functionPressed,fieldValues)
        if fieldValues is not None: #this means extra params were required but the input was cancelled, so dont add
            result = self.testSeq.add_to_sequence(functionPressed, fieldValues)
            if result[0] is not PASS:
               easygui.msgbox("Unable to add test "+result[1])
            else:
                self.updateOutputPanel() #update UI
                

    def updateOutputPanel(self):
        self.outputList.clear()
        if(len(self.testSeq.test_sequence_with_params) > 0):
            self.outputList.setColumnCount(4)
            self.outputList.setRowCount(len(self.testSeq.test_sequence_with_params))
        else:
            self.outputList.setRowCount(0)
            self.outputList.setRowCount(0)
        
        i = 0
        for seq in self.testSeq.test_sequence_with_params:
            name = seq[0]
            params = " -> "
            for p in seq[1]:
                params = params+str(p)+" ,"
            item = QTableWidgetItem(str(i+1)+" : "+name+params)
            self.outputList.setItem(i,0,item)
            
            itemMoveUp = QTableWidgetItem("")
            icon_moveup = QIcon('up.png')
            itemMoveUp.setIcon(icon_moveup)
            self.outputList.setItem(i,1,itemMoveUp)

            itemMoveDown = QTableWidgetItem("")
            icon_movedown = QIcon('down.png')
            itemMoveDown.setIcon(icon_movedown)
            self.outputList.setItem(i,2,itemMoveDown)

            itemDelete = QTableWidgetItem("")
            icon_delete= QIcon('delete.png')
            itemDelete.setIcon(icon_delete)
            self.outputList.setItem(i,3,itemDelete)
            i=i+1
        #self.outputBoxLayout.addWidget(QPushButton(elf.testSeq[))
        #refresh the output panel
        return
                
    def cellClicked(self, clickedIndex):
        row = clickedIndex.row()
        
        if clickedIndex.column() == 0:
            return #do nothing, may be in future edit the parameters
        elif clickedIndex.column() == 1: #move up
            self.testSeq.move_item_up(row)
        elif clickedIndex.column() == 2: #move down
            self.testSeq.move_item_down(row)
        elif clickedIndex.column() == 3: #delete
            self.testSeq.delete_item(row)
        else:
            return
            
        #update table with latest data, since it changed
        self.updateOutputPanel()

    def saveClicked(self):
        #self.nameInput = QLineEdit()
        #self.developerInput = QLineEdit())
        #self.descriptionInput = QLineEdit()
        self.testSeq.test_name = self.nameInput.text()
        self.testSeq.test_description = self.developerInput.text()
        self.testSeq.test_description = self.descriptionInput.text()
        err = self.testSeq.save()

        if(err[0] != PASS):
            easygui.msgbox(err[1])
        else:
            easygui.msgbox("Saved Successfully")
            self.loadAllFiles()
            self.updateAllFileUI()
            self.startNew()

    def updateAllFileUI(self):
        self.allFileList.clear()
        if(len(self.allFiles) > 0):
            self.allFileList.setColumnCount(2)
            self.allFileList.setRowCount(len(self.allFiles))
        else:
            self.allFileList.setColumnCount(0)
            self.allFileList.setRowCount(0)

        i = 0
        for file in self.allFiles:
            item = QTableWidgetItem(file)
            self.allFileList.setItem(i,0,item)

            itemDelete = QTableWidgetItem("")
            icon_delete= QIcon('delete.png')
            itemDelete.setIcon(icon_delete)
            self.allFileList.setItem(i,1,itemDelete)
            
            i = i+1
            
        return
    
    def loadAllFiles(self):
        path =  "tests/sequences/"
        self.allFiles = []
        for (dirpath, dirnames, filenames) in os.walk(path):
             for file in filenames:
                 if file.endswith(".seq"):
                     self.allFiles.append(file[:-4])
        return

    
    def allFileClicked(self, clickedIndex):
        row = clickedIndex.row()
        col = clickedIndex.column()
        path =  "tests/sequences/"
        
        if col == 0: #do nothign with a single click
            return
        elif col == 1: #delete
            #delete File
            nameOnly = self.allFileList.item(row,0).text()
            name = path+nameOnly+".seq"
            try:
                os.remove(name)
                self.allFiles.pop(row)
                easygui.msgbox("Successfully Deleted " + nameOnly)
            except:
                easygui.msgbox("Error Deleting sequence " + nameOnly)
            self.updateAllFileUI()
        return

    def allFileDoubleClicked(self,clickedIndex):
        row = clickedIndex.row()
        col = clickedIndex.column()
        
        if col == 0: #do nothign with a single click
            nameOnly = self.allFileList.item(row,0).text()
            self.testSeq = TestSequence()
            self.testSeq.load(nameOnly)
            self.nameInput.setText(self.testSeq.test_name)
            self.developerInput.setText(self.testSeq.test_developer)
            self.descriptionInput.setText(self.testSeq.test_description)
            self.updateOutputPanel()
            return
        elif col == 1: #delete
            # do nithign on a doubel click on delete
            return

    def refreshAllFiles(self):
        self.loadAllFiles()
        self.updateAllFileUI()
        
    def startNew(self):
        self.testSeq = TestSequence()
        self.nameInput.setText("")
        self.developerInput.setText("")
        self.descriptionInput.setText("")
        self.updateOutputPanel()
        return
        
if __name__ == '__main__':
    
    app = QApplication(sys.argv)
    ex = TestSeqCreator()
    sys.exit(app.exec_())
