import sys
from PyQt5.QtWidgets import QMainWindow, QTextEdit, QAction, QApplication
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QHBoxLayout, QGroupBox, QDialog, QVBoxLayout, QGridLayout, QToolButton, QStyleOptionToolButton

from CreateNewTestSuite import TestSuiteCreator
from CreateNewTestSeq import TestSeqCreator
from CalibDataHandler import CalibData
from ClockTests import *
import easygui
import math
from TimeTeller import TimeTeller

#camera stuff
import cv2

class CameraCalib(QMainWindow):
    
    def __init__(self, parent=None):
        super().__init__(parent)
        self.parent = parent
        self.title = 'Calibrate Camera'
        self.left = 100
        self.top = 100
        self.width = 500
        self.height = 300
        self.initUI()

        self.calibData = CalibData()
        self.image = None
        #insertCalib(self,twelve, three ,six, nine)
        #setNumberOfClocks
        #performSanityCheck
        #saveCalibData
        #loadCalibData
        
    def initUI(self):               

        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        
        
        exitAction = QAction(QIcon('exit24.png'), 'Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(self.close)

        self.statusBar()
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(exitAction)

        toolbar = self.addToolBar('Exit')
        toolbar.addAction(exitAction)

        buttonGroup = self.createButtons()
        self.setCentralWidget(buttonGroup)
        
        self.statusBar().showMessage("Ready!")
        self.show()

    def createButtons(self):
        horizontalGroupBox = QGroupBox()
        layout = QGridLayout()
        
        check_calib_but = QPushButton()
        check_calib_but.setText("Check Current\nCalibration")
        check_calib_but.setFixedSize(100, 100)
        check_calib_but.released.connect(self.showCurrentCalib)
        
        
        update_calib_but = QPushButton()
        update_calib_but.setText("Start Calibration")
        update_calib_but.setFixedSize(100, 100)
        update_calib_but.released.connect(self.startCalibration)
        
        layout.addWidget(check_calib_but,0,0) 
        layout.addWidget(update_calib_but,0,1) 
 
        horizontalGroupBox.setLayout(layout)

        return horizontalGroupBox

    def showCurrentCalib(self):
        result = self.calibData.loadCalibData()
        if ( result[0] != PASS):
            easygui.msgbox(result[1])
            return

        #show calib data on current image
        image = self.get_image()
        if ( image == None):
            easygui.msgbox("Error with camera")
            return

        cv2.namedWindow('Current Calib Data')
        

        #display current calib data
        for points in self.calibData.calibData:
            #draw lines from 12 to 6 and from 3 to 9
            #also draw a circle from center point
            obj = TimeTeller(image, points)
            timeOnFace = obj.readTime()
            
            cv2.line(image,points['12'],points['6'],(0, 255, 0), thickness = 2)
            cv2.line(image,points['3'],points['9'],(0, 255, 0), thickness=2)

            centerx = int((points['12'][0] + points['6'][0])/2)
            centery = int((points['3'][1] + points['9'][1])/2)

            #dist = sqrt ( (x2-x1)^2 + (y2-y1)^2 )
            #get distance between 12 and center so that we can draw a radius
            dist = math.sqrt( (centerx - points['12'][0])**2 + (centery - points['12'][1])**2 )
            dist = int(dist)
            cv2.circle(image, (centerx,centery),dist, (0, 255, 0), thickness=2)
            
            

        cv2.imshow('Current Calib Data',image)    
        return
        
        print("showCurrentCalib")
        return
    
    def startCalibration(self):
        print("startCalibration")
        #ask for number of clocks
        msg = "Plese enter number of clocks you will use for testing"
        title = "Number of Clocks"
        fieldNames = ["Number of Clocks to Calibrate: "]
        fieldValues = []
        fieldValues = easygui.multenterbox(msg,title, fieldNames)
        while True:
            if(fieldValues == None):
                self.calibData.setNumberOfClocks(0)
                break
            errmsg = ""
            if fieldValues[0].strip() == "":
                errmsg = "Please Enter a valid Number of clocks (0-30)"
            else:
                try:
                    val = int(fieldValues[0])
                    if val<=0 or val>30:
                        raise NameError('')
                except:
                    errmsg = "Please Enter a valid Number of clocks (0-30)"
            if errmsg == "":
                self.calibData.setNumberOfClocks(int(fieldValues[0]))
                break
            fieldValues = easygui.multenterbox(errmsg, title, fieldNames, fieldValues)

        if self.calibData.numberOfClocks > 0:
            #get inputs for each clock
            
            #take picture and display picture
            image = self.get_image()
            if( image == None): #error while getting image
                easygui.msgbox("Error with Camera! " )
                return False

            self.current_clock = 0
            self.current_12 = 0
            self.current_3 = 0
            self.current_6 = 0
            self.current_9 = 0
            self.click_count = 0
            #display instructions
            easygui.msgbox("You will now be shown an image of the clock.\n You will have to select the 12,3,6 and 9 dots in\n that order for each clock.")
            cv2.namedWindow('Current Clock Wall')
            cv2.setMouseCallback('Current Clock Wall',self.pointSelected)
            cv2.imshow('Current Clock Wall',image)
            easygui.msgbox(" Calib clock number: "+str(self.current_clock +1)+". Click OK to get started" )

            #calibration complete

    def pointSelected(self,event,x,y,flags,param):
        if event == cv2.EVENT_LBUTTONDOWN: #left button clicked, select point
            
            self.click_count+=1

            if self.click_count == 1:
                self.current_12 = (x,y)
            elif self.click_count == 2:
                self.current_3 = (x,y)
            elif self.click_count == 3:
                self.current_6 = (x,y)
            elif self.click_count == 4:
                self.current_9 = (x,y)
                self.calibData.insertCalib(self.current_12, self.current_3, self.current_6, self.current_9)
                self.current_clock += 1
                self.click_count = 0
                if (self.current_clock != self.calibData.numberOfClocks):
                    easygui.msgbox(" Calib clock number: "+str(self.current_clock +1)+". Click OK to get started." )
            
            if (self.current_clock == self.calibData.numberOfClocks):
                #save current calibration and close image
                if(self.calibData.saveCalibData()[0] == PASS):
                    easygui.msgbox(" Saved Calib Data! " )
                else:
                    easygui.msgbox(" Error while saving calib data! " )
                self.current_clock = 0
                self.current_12 = 0
                self.current_3 = 0
                self.current_6 = 0
                self.current_9 = 0
                self.click_count = 0
                cv2.destroyAllWindows()
        else: #only process clicks
            return #do nothing

    def get_image(self):
        filename = "working_image.jpg"
        im = None
        try:
            camera = cv2.VideoCapture(0)
            camera.set(cv2.CAP_PROP_FRAME_WIDTH,1920)
            camera.set(cv2.CAP_PROP_FRAME_HEIGHT ,1080)
            retval, im = camera.read()
            cv2.imwrite(filename, im)
            del(camera)
        except e:
            im = None

        return im
        
        
if __name__ == '__main__':
    
    app = QApplication(sys.argv)
    ex = CameraCalib()
    sys.exit(app.exec_())
