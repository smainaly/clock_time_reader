import cv2
import numpy as np
import math
import matplotlib.pyplot as plt
#import win32api, win32con

def get_image():
    # read is the easiest way to get a full image out of a VideoCapture object.
    retval, im = camera.read()
    return im

filename = 'temp.jpg'
camera_port = 0

camera = cv2.VideoCapture(camera_port)

camera.set(cv2.CAP_PROP_FRAME_WIDTH,1920)
camera.set(cv2.CAP_PROP_FRAME_HEIGHT ,1080)

camera_capture = get_image()
cv2.imwrite(filename, camera_capture)
del(camera)

image = cv2.imread(filename)
output = image.copy()
faceOnly = None

#new image for just hands
size = image.shape
handsOnly= np.ones(size, dtype=np.uint8)

circleOnly = np.zeros(size, dtype=np.uint8)

#convert to gray scale
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

ret,thresh1 = cv2.threshold(gray,127,255,cv2.THRESH_BINARY)
ret,thresh2 = cv2.threshold(gray,127,255,cv2.THRESH_BINARY_INV)
ret,thresh3 = cv2.threshold(gray,127,255,cv2.THRESH_TRUNC)
ret,thresh4 = cv2.threshold(gray,127,255,cv2.THRESH_TOZERO)
ret,thresh5 = cv2.threshold(gray,127,255,cv2.THRESH_TOZERO_INV)



#cv2.imshow("screen",  np.hstack([thresh1,thresh2,thresh3,thresh4,thresh5]))
#cv2.waitKey(0)

#using thresh4 for now
kernel = np.ones((5,5),np.uint8)
opening = cv2.morphologyEx(thresh3, cv2.MORPH_OPEN, kernel)
closing = cv2.morphologyEx(thresh3, cv2.MORPH_CLOSE, kernel)

cv2.imshow("output",  np.hstack([opening,closing]))
cv2.waitKey(0)


# we have 2 images now, openign and closing, extract circle using one of them
circles = cv2.HoughCircles(closing, cv2.HOUGH_GRADIENT, 1.2, 1000)
circles = np.round(circles[0, :]).astype("int")
centerx = centery = radius = 0
for (b, a, r) in circles:
    print(a,b,r)
    cv2.circle(output, (b, a), r, (0, 255, 0), 4)
    ny,nx=image.shape[:2]
    y,x = np.ogrid[-a:ny-a,-b:nx-b]
    mask = x*x + y*y <=r*r
    array = np.zeros((ny,nx),np.uint8)
    array[mask]= 255
    faceOnly = cv2.bitwise_and(thresh1,thresh1,mask = array)

    #print(b,a,r)
    centerx = b
    centery = a
    radius = r



#cv2.circle(circleOnly, (centerx, centery), radius-170, (255, 255, 255), 1)
#cv2.circle(circleOnly, (centerx, centery), radius-270, (255, 255, 255), 1)
#cv2.circle(circleOnly, (centerx, centery), radius-195, (255, 255, 255), 1)

#cv2.circle(faceOnly, (centerx, centery), radius-170, (0, 0, 0), 1)
#cv2.circle(faceOnly, (centerx, centery), radius-270, (0, 0, 0), 1)
#cv2.circle(faceOnly, (centerx, centery), radius-195, (0, 0, 0), 1)

#print(circleOnly.shape)
#print(output.shape)
#print(faceOnly.shape)




#cv2.imshow("res", faceOnly)
#cv2.imshow("ress", circleOnly)
#cv2.imshow("output", output)
#cv2.waitKey(0)


# GET LOCATION FOR 12 and 6
#-----------------START----------------------------------
# get first 2 mouse clicks
firstClickDone = False
secondClickDone = False
def getMouseClick(event,x,y,flags,param):
    global mouseX1,mouseY1,mouseX2,mouseY2
    global secondClickDone, firstClickDone
    
    global gx,gy
    gx,gy = x,y
    
    if event == cv2.EVENT_LBUTTONDOWN:
        if not firstClickDone:
            mouseX1,mouseY1 = x,y
            firstClickDone = True
            print(mouseX1,mouseY1)
        else:
            mouseX2,mouseY2 = x,y
            print(mouseX2,mouseY2)
            secondClickDone = True

cv2.namedWindow('inputpoints')
cv2.setMouseCallback('inputpoints',getMouseClick)
cv2.imshow('inputpoints',faceOnly)

while(1):
    k = cv2.waitKey(1) & 0xFF

    if firstClickDone and secondClickDone:
        break

    if k == 13:
        if not firstClickDone:
            mouseX1,mouseY1 = gx,gy
            print(mouseX1,mouseY1)
            firstClickDone = True
        else:
            mouseX2,mouseY2 = gx,gy
            print(mouseX2,mouseY2)
            secondClickDone = True
    elif k==97: #left
        gx = gx-1
        a,b = win32api.GetCursorPos()
        win32api.SetCursorPos((a-1,b))
    elif k==119: #up
        gy=gy-1
        a,b = win32api.GetCursorPos()
        win32api.SetCursorPos((a,b-1))
    elif k==100: #right
        gx=gx+1
        a,b = win32api.GetCursorPos()
        win32api.SetCursorPos((a+1,b))
    elif k==115: #down
        gy=gy+1
        a,b = win32api.GetCursorPos()
        win32api.SetCursorPos((a,b+1))


cv2.destroyAllWindows()
#-----------------END----------------------------------

#iterate through first circle find the circle
angle = 0.005
startingblack = False
r = radius-170

xpoints = []
ypoints = []


blackInProgress = False
hands = []
thisone = []

while (angle <= 2*3.14):
    pointy = int(r*np.cos(angle))
    pointx = int(r*np.sin(angle))

        
    if(faceOnly[centery+pointy][centerx+pointx] == 0):
        output[centery+pointy][centerx+pointx] = (0, 255, 0)
        thisone.append((centerx+pointx,centery+pointy))
        blackInProgress = True
    else:
        if blackInProgress:
            hands.append(thisone)
            thisone = []
        blackInProgress = False
        
    angle+=0.00005

for hand in hands:
    #get center of this hand
    sumx = 0
    sumy = 0
    for x,y in hand:
        sumx += x
        sumy += y

    #print("POINT1")
    #print(int(sumx/len(hand)), int(sumy/len(hand)))
    print(sumx, sumy)
    output[int(sumy/len(hand))][int(sumx/len(hand))] = (255, 0, 0)
    
        

#iterate through first circle find the circle
angle = 0.005
startingblack = False
r = radius-195

xpoints = []
ypoints = []


blackInProgress = False
hands = []
thisone = []

while (angle <= 2*3.14):
    pointy = int(r*np.cos(angle))
    pointx = int(r*np.sin(angle))

        
    if(faceOnly[centery+pointy][centerx+pointx] == 0):
        output[centery+pointy][centerx+pointx] = (0, 255, 0)
        thisone.append((centerx+pointx,centery+pointy))
        blackInProgress = True
    else:
        if blackInProgress:
            hands.append(thisone)
            thisone = []
        blackInProgress = False
        
    angle+=0.00005

for hand in hands:
    #get center of this hand
    sumx = 0
    sumy = 0
    for x,y in hand:
        sumx += x
        sumy += y

    #print("POINT2")
    #print(int(sumx/len(hand)), int(sumy/len(hand)))
    print(sumx, sumy)
    output[int(sumy/len(hand))][int(sumx/len(hand))] = (255, 0, 0)

#iterate through second circle find the circle
angle = 0.005
startingblack = False
r = radius-270

xpoints = []
ypoints = []


blackInProgress = False
hands = []
thisone = []

while (angle <= 2*3.14):
    pointy = int(r*np.cos(angle))
    pointx = int(r*np.sin(angle))

        
    if(faceOnly[centery+pointy][centerx+pointx] == 0):
        output[centery+pointy][centerx+pointx] = (0, 255, 0)
        thisone.append((centerx+pointx,centery+pointy))
        blackInProgress = True
    else:
        if blackInProgress:
            hands.append(thisone)
            thisone = []
        blackInProgress = False
        
    angle+=0.00005

for hand in hands:
    #get center of this hand
    sumx = 0
    sumy = 0
    for x,y in hand:
        sumx += x
        sumy += y

    #print("POINT3")
    #print(int(sumx/len(hand)), int(sumy/len(hand)))
    print(sumx, sumy)
    output[int(sumy/len(hand))][int(sumx/len(hand))] = (255, 0, 0)



cv2.line(output,(337, 636),(385, 549),(0, 255, 0)) #second
cv2.line(output,(280, 582),(359, 520),(0, 255, 0)) #minute
cv2.line(output,(284, 399),(353, 427),(0, 255, 0)) #hour

#draw the reference line
cv2.line(output,(mouseX1, mouseY1),(mouseX2, mouseY2),(0, 255, 0))

refAngle = np.arctan2(mouseY1 - mouseY2,mouseX1 - mouseX2)
refAngle = (refAngle if refAngle>0 else 2*np.pi+refAngle)*360/(2*np.pi)
#calculate angles
hourAngle_ = np.arctan2(399 - 427,284 - 353)
hourAngle_ = (hourAngle_ if hourAngle_>0 else 2*np.pi+hourAngle_)*360/(2*np.pi)
hourAngle = hourAngle_ - refAngle
hourAngle = hourAngle if hourAngle>0 else hourAngle+360
print("Hour Angle = %f"%hourAngle)


#calculate angles
minAngle_ = np.arctan2(582 - 520,280 - 359)
minAngle_ = (minAngle_ if minAngle_>0 else 2*np.pi+minAngle_)*360/(2*np.pi)
minAngle =  minAngle_ - refAngle
minAngle = minAngle if minAngle>0 else minAngle+360
print("Min Angle = %f"%minAngle)

#calculate angles
secAngle_ = np.arctan2(636 - 549,337 - 385)
secAngle_ = (secAngle_ if secAngle_>0 else 2*np.pi+secAngle_)*360/(2*np.pi)
secAngle = secAngle_ - refAngle
secAngle = secAngle if secAngle>0 else secAngle+360
print("Sec Angle = %f"%secAngle)



cv2.imshow("output", output)
#cv2.imshow("output2", faceOnly)

hour = int(np.floor(hourAngle/360 * 12))
minute = int(np.floor(minAngle/360 *60))
second = int(np.floor(secAngle/360 *60))
print("HH:MM:SS =  %d:%d:%d"%(hour,minute,second))

cv2.waitKey(0)
'''
#edges only from faceOnly
edges = cv2.Canny(faceOnly,500,500,apertureSize = 3)
#cv2.imshow("res", edges)
#cv2.waitKey(0)


#getting clock hands from edges
minLineLength = 100
maxLineGap = 0
ret, edges = cv2.threshold(edges,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
lines = cv2.HoughLinesP(edges,1,np.pi/180,10,minLineLength,maxLineGap)

for x in range(0, len(lines)):
    for x1,y1,x2,y2 in lines[x]:
        cv2.line(handsOnly,(x1,y1),(x2,y2),(255,255,255),2)
        
print(x+1)
#cv2.imshow("output",handsOnly )
#cv2.waitKey(0)
'''









cv2.destroyAllWindows()
