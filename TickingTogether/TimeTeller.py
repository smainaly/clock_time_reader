import cv2
import numpy as np
import math

class TimeTeller():

    def __init__(self, image = None, calibData=None):

        #self.image = None
        #self.time = {'HH':12,'MM':00,'SS':00}
        #self.calibdata = {'12':0,'2':0,'3':0,'9':0}

        self.image = image.copy() #creating copy so that original is not altered
        self.image_color = image.copy()
        self.time = None
        self.calibData = calibData


    def transformImage(self):
        kernel = np.ones((5,5),np.uint8)
        self.image = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)

        #self.image = cv2.GaussianBlur(self.image,(5,5),0)
        
        #ret,self.image = cv2.threshold(self.image,175,250,cv2.THRESH_BINARY)
        (thresh, self.image) = cv2.threshold(self.image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        self.image = cv2.threshold(self.image, thresh, 255, cv2.THRESH_BINARY)[1]
        #self.image = cv2.morphologyEx(self.image, cv2.MORPH_CLOSE, kernel)
        cv2.imshow("outprfdgdfgut",  self.image)

    def iterateThroughRadius(self,r,centerx, centery):
        angle = 0.005
        startingblack = False
        xpoints = []
        ypoints = []
        blackInProgress = False
        hands = []
        thisone = []
        
        while (angle <= 2*3.14):
            pointy = int(r*np.cos(angle))
            pointx = int(r*np.sin(angle))
            if(self.image[centery+pointy][centerx+pointx] == 0):
                #self.image[centery+pointy][centerx+pointx] = (0, 255, 0)
                thisone.append((centerx+pointx,centery+pointy,angle))
                blackInProgress = True
            else:
                if blackInProgress:
                    hands.append(thisone)
                    thisone = []
                blackInProgress = False
            angle+=0.00005

        handsInfo = []
        for hand in hands:
            sumx = 0
            sumy = 0
            sumangle = 0
            thickness = 0
            
            for y,x,angle in hand:
                sumx += x
                sumy += y
                sumangle += angle
                thickness += 1
                
            handInfo = { 'x': int(sumx/len(hand)),
                         'y': int(sumy/len(hand)),
                         'angle': math.degrees(sumangle/len(hand)),
                         'thickness': thickness
                        }
            handsInfo.append(handInfo)
            
        return handsInfo

    def getThinnestHand(self, handset):
        thinnest_index = 0
        for i in range(1, len(handset)):
            if handset[i]['thickness'] < handset[thinnest_index]['thickness']:
                thinnest_index = i
        return thinnest_index

    def getHandWithMostSimilarAngle(self, handset, angle):
        most_similar_index = 0
        most_similar_angle_diff = math.fabs(angle - handset[most_similar_index]['angle'])

        for i in range(1, len(handset)):
            current_angle_diff =  math.fabs(angle - handset[i]['angle'])
            if(current_angle_diff < most_similar_angle_diff):
                most_similar_index = i
                most_similar_angle_diff = current_angle_diff
        return most_similar_index
            
    
    def getTimeFromHands(self, handset1, handset2, handset3):
        time_in_clock = None

        minute_hand = []
        hour_hand = []
        second_hand = []
        #handset1 is easy, either there is one hand or 2 hands
        # 2 scenarios, secodn hand is too thin, or there is overlap.
        # so if 1 hand, then take that as the minute hand, if 2 hands ignore the thinner hand, done deal

        if len(handset1) == 2:
            thickerhand = handset1[0] if handset1[0]['thickness'] > handset1[1]['thickness'] else handset1[0] #just savign thicker hand
        if len(handset1) == 1:
            thickerhand = handset1[0] #we will get there only if there is only one hand detected in there
        else:
            return None # this means that there were 0 hands or more than 2 hands detected, both of which are not possible
        minute_hand.append(thickerhand)
    
        if (len(handset2)) == 3:
            #this is the easier case, where all three hands were detected
            thinnest_index = self.getThinnestHand(handset2)
            second_hand.append(handset2[thinnest_index])
            handset2.pop(thinnest_index) # remove the minute

            #to get minute hand, we know one minute angle, so get th eone with the most similar angle
            most_similar_to_minute_index = self.getHandWithMostSimilarAngle(handset2,minute_hand[0]['angle']  )
            minute_hand.append(handset2[most_similar_to_minute_index])
            handset2.pop(most_similar_to_minute_index)

            #the remaining one is the hour hand
            hour_hand.append(handset2[0])
        else: #need to handle cases of 2 and 1, may eb overlapping hands
            return None

    
        if (len(handset3)) == 3:
            #this is the easier case, where all three hands were detected
            thinnest_index = self.getThinnestHand(handset3)
            second_hand.append(handset3[thinnest_index])
            handset3.pop(thinnest_index) # remove the minute

            #to get minute hand, we know one minute angle, so get th eone with the most similar angle
            most_similar_to_minute_index = self.getHandWithMostSimilarAngle(handset3,minute_hand[1]['angle']  )
            minute_hand.append(handset3[most_similar_to_minute_index])
            handset3.pop(most_similar_to_minute_index)

            #the remaining one is the hour hand
            hour_hand.append(handset3[0])
        else:
            return None
            
        #print(minute_hand)
        #print(second_hand)
        #print(hour_hand)

        #so we are here, we have 2 poitns for hour, minute and second, if not then we wound be here
        refAngle = np.arctan2(self.calibData['12'][1] - self.calibData['6'][1],self.calibData['12'][0] - self.calibData['6'][0])
        refAngle = (refAngle if refAngle>0 else 2*np.pi+refAngle)*360/(2*np.pi)

        #hour angle
        hourAngle_ = np.arctan2(hour_hand[0]['x'] - hour_hand[1]['x'],hour_hand[0]['y'] - hour_hand[1]['y'])
        hourAngle_ = (hourAngle_ if hourAngle_>0 else 2*np.pi+hourAngle_)*360/(2*np.pi)
        hourAngle = hourAngle_ - refAngle
        hourAngle = hourAngle if hourAngle>0 else hourAngle+360
        #minute angle
        minAngle_ = np.arctan2(minute_hand[0]['x'] - minute_hand[1]['x'],minute_hand[0]['y'] - minute_hand[1]['y'])
        minAngle_ = (minAngle_ if minAngle_>0 else 2*np.pi+minAngle_)*360/(2*np.pi)
        minAngle =  minAngle_ - refAngle
        minAngle = minAngle if minAngle>0 else minAngle+360
        #second angle
        secAngle_ = np.arctan2(second_hand[0]['x'] - second_hand[1]['x'],second_hand[0]['y'] - second_hand[1]['y'])
        secAngle_ = (secAngle_ if secAngle_>0 else 2*np.pi+secAngle_)*360/(2*np.pi)
        secAngle = secAngle_ - refAngle
        secAngle = secAngle if secAngle>0 else secAngle+360

        hour = int(hourAngle/360 * 12)
        minute = int(minAngle/360 *60)
        minute = minute if minute<60 else 0
        second = int(secAngle/360 *60)
        second = second if second<60 else 0

        print(refAngle, hourAngle, minAngle, secAngle)
        print(hour, minute, second)

        #take care of distortion
        # if second is whthin + or - 15 seconds of 60, and the minute ahnd is borderline then adjust minute accordingly        
        
            
        return time_in_clock
    
    
    def readTime(self, image = None):
        if image != None:
            self.image = image.copy() #making an image coz we need to edit it
        
        if (self.image == None):
            return False

        self.transformImage()
        
        centerx = int((self.calibData['12'][0] + self.calibData['6'][0])/2)
        centery = int((self.calibData['3'][1] + self.calibData['9'][1])/2)
        radius = int(math.sqrt( (centerx - self.calibData['12'][0])**2 + (centery - self.calibData['12'][1])**2 ) )

        cv2.circle(self.image_color, (centerx,centery),int(radius*0.93), (0, 255, 0), thickness=2)
        cv2.circle(self.image_color, (centerx,centery),int(radius*0.65), (0, 255, 0), thickness=2)
        cv2.circle(self.image_color, (centerx,centery),int(radius*0.47), (0, 255, 0), thickness=2)
        
        cv2.imshow("output",  self.image_color)
      
        hands_set1 = self.iterateThroughRadius(int(radius*0.93), centerx, centery) #shoudl have 2 hands
        hands_set2 = self.iterateThroughRadius(int(radius*0.65), centerx, centery) #should have 3 hands
        hands_set3 = self.iterateThroughRadius(int(radius*0.47), centerx, centery) # should have 3 hands

        print(len(hands_set1), len(hands_set2), len(hands_set3))
        
        time_in_clock = self.getTimeFromHands(hands_set1, hands_set2, hands_set3)
        
    
        
        

        
        

