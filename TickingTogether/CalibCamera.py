import sys
from PyQt5.QtWidgets import QMainWindow, QTextEdit, QAction, QApplication
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QHBoxLayout, QGroupBox, QDialog, QVBoxLayout, QGridLayout, QToolButton, QStyleOptionToolButton

from CreateNewTestSuite import TestSuiteCreator
from CreateNewTestSeq import TestSeqCreator
from CalibDataHandler import CalibData
from ClockTests import *
import easygui
import math
from TimeTeller import TimeTeller
import time

#camera stuff
import cv2

def get_image(self):
    filename = "working_image"
    im = None
    camera = cv2.VideoCapture(0)
    camera.set(cv2.CAP_PROP_FRAME_WIDTH,1920)
    camera.set(cv2.CAP_PROP_FRAME_HEIGHT ,1080)
        

    for i in range(0,20):
        retval, im = camera.read()
        cv2.imwrite(filename+str(i)+".jpg", im)
        time.sleep(3)

    del(camera)
        
def calibrate():
    
