from ClockTests import *
import os
import pickle

class TestSequence:
    def __init__(self, name="", developer = "", description = ""):
        self.test_name = name
        self.test_developer = developer
        self.test_description = description
        self.test_sequence_with_params = [] #each item here will be a list, ["Test Name", ( param1, param2, ) ]
        
    def add_to_sequence(self,test_name, *params):
        params = tuple(*params)
        test_with_params_and_order = [test_name , params]
        test_check = self.check_params_for_test(test_name, *params)
        if(test_check[0] == PASS ): 
            self.test_sequence_with_params.append(test_with_params_and_order)
        return(test_check)

    def check_params_for_test(self,test_name,*params):
        if test_name not in Tests:
            return((INVALID_TEST, "Test with that name is not available" ))

        function_to_call = Tests[test_name]
        function_to_call = function_to_call[0]
        
        return(function_to_call(FUNCTION_TEST_PARAMS,*params))


    def move_item_up(self,index):
        if(index == 0):
            return((FAIL, "Already First Item."))
        if( index < 0 or index > len(self.test_sequence_with_params)-1):
            return((FAIL, "Invalid item in list."))
        self.test_sequence_with_params.insert(index-1,self.test_sequence_with_params.pop(index))
        return( (PASS, "No Errors" ) )

    def move_item_down(self,index):
        if(index == len(self.test_sequence_with_params)-1):
            return((FAIL, "Already Last Item."))
        if( index < 0 or index > len(self.test_sequence_with_params)-1):
            return((FAIL, "Invalid item in list."))
        self.test_sequence_with_params.insert(index+1,self.test_sequence_with_params.pop(index))
        return( (PASS, "No Errors" ) )


    def delete_item(self,index):
        if( index < 0 or index > len(self.test_sequence_with_params)-1):
            return((FAIL, "Invalid item in list."))
        self.test_sequence_with_params.pop(index)
        return( (PASS, "No Errors" ) )


    def save(self, over_ride = False):
        if self.test_name == None or len(self.test_name) == 0:
            return( (FAIL, "Test Sequence Name Missing" ) )

        filename = "tests/sequences/"+self.test_name+".seq"
        if not over_ride and os.path.isfile(filename):
            return( (ALREADY_EXISTS, "Sequence with that name already exists. It has to be Unique." ) )

        if len(self.test_sequence_with_params) == 0:
            return( (FAIL, "Sequence is Empty. Add some steps to sequence." ) )
        
        try:
            output_file = open(filename,"wb")
            pickle.dump(self, output_file, pickle.HIGHEST_PROTOCOL)
        except:
            return( (FAIL, "Error while saving Sequence."  ) )
        
        return( (PASS, "Test Sequence Saved." ) )

    def load(self,name=None):
        self.test_name = name if (name is not None) else self.test_name
        if self.test_name == None or self.test_name == "":
            return( (FAIL, "No Sequence Name Given" ) )

        filename = "tests/sequences/"+self.test_name+".seq"
        if not os.path.isfile(filename):
            return( (FAIL, "Sequence with that name does not exist" ) )

        try:
            self.__dict__ = pickle.load( open( filename, "rb" ) ).__dict__
        except:
            return( (FAIL, "Error While Loading Sequence" ) )

        
        return( (PASS, "Sequence Loaded Successfully" ) )
        
        return
        

    
        
