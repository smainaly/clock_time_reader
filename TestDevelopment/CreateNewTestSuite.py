import sys
from PyQt5.QtWidgets import QMainWindow, QTextEdit, QAction, QApplication
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QScrollArea, QApplication, QWidget, QPushButton, QHBoxLayout, QGroupBox, QDialog, QVBoxLayout, QGridLayout, QToolButton, QStyleOptionToolButton
from PyQt5.QtWidgets import QLabel, QLineEdit, QListWidget, QListWidgetItem, QTableWidget, QTableWidgetItem, QAbstractItemView
import easygui
from ClockTests import *
from TestSuite import *
import os

class TestSuiteCreator(QMainWindow):
    
    def __init__(self, parent=None):
        super().__init__(parent)
        self.parent = parent
        self.title = 'Add/Edit/Delete Test Suite'
        self.left = 100
        self.top = 100
        self.width = 750
        self.height = 700
        self.initUI()
        self.testSuite = TestSuite()
        
    def initUI(self):               
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        
        exitAction = QAction(QIcon('exit24.png'), 'Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(self.close)

        self.statusBar()
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(exitAction)

        toolbar = self.addToolBar('Exit')
        toolbar.addAction(exitAction)

        topInputGroup = self.createTopInputs()
        
        SeqGroup = self.createSeqListPanel()
        self.loadAllSequences()
        self.updateAllSeqUI()
        
        outputPanel = self.createOutputPanel()
        buttonPanel = self.createButtonPanel()
        allFilePanel = self.createAllFilePanel()
        allfilebuttonPanel = self.createAllFileButtonPanel()
        
        mainGroup = QGroupBox()
        mainLayout = QGridLayout()

        mainLayout.addWidget(allFilePanel, 0,0,2,1)
        mainLayout.addWidget(allfilebuttonPanel,2,0)
        mainLayout.addWidget(topInputGroup,0,1)
        mainLayout.addWidget(SeqGroup,1,1)
        mainLayout.addWidget(outputPanel,1,2)
        mainLayout.addWidget(buttonPanel, 2,2)
        
        
        mainGroup.setLayout(mainLayout)
        
        self.setCentralWidget(mainGroup)
        
        self.statusBar().showMessage("Ready!")

        self.loadAllFiles()
        self.updateAllFileUI()
        
        self.show()

    def createTopInputs(self):
        topInputsBox = QGroupBox()
        topInputsBox.setMaximumWidth(300)
        topInputsBox.setMinimumWidth(300)
        layout = QGridLayout()

        nameLabel = QLabel("Name:")
        self.nameInput = QLineEdit()
        developerLabel = QLabel("Developer:")
        self.developerInput = QLineEdit()
        descriptionLabel = QLabel("Description:")
        self.descriptionInput = QLineEdit()
        #descriptionInput = QTextEdit()
        #descriptionInput.setFixedHeight(40)
        
        layout.addWidget(nameLabel, 0, 0)
        layout.addWidget(self.nameInput, 0, 1)
        layout.addWidget(developerLabel, 1, 0)
        layout.addWidget(self.developerInput, 1, 1)
        layout.addWidget(descriptionLabel, 2, 0)
        layout.addWidget(self.descriptionInput, 2, 1)
        topInputsBox.setLayout(layout)
        return topInputsBox

    def createSeqListPanel(self):
        self.allSeqList = QTableWidget(0,3)
        self.allSeqList.verticalHeader().setVisible(False)
        self.allSeqList.horizontalHeader().setVisible(False)

        self.allSeqList.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.allSeqList.setColumnWidth(0,20)
        self.allSeqList.setColumnWidth(1,250)
        self.allSeqList.setColumnWidth(2,20)
        self.allSeqList.itemClicked.connect(self.addNewSeqToSuite)
        self.allSeqList.itemDoubleClicked.connect(self.addToSeqDoubleClicked)

        self.allSeqList.setShowGrid(False)
        
        layout = QVBoxLayout()
        self.allSeqList.setMinimumWidth(230)
        self.allSeqList.setLayout(layout)
        return self.allSeqList
    
    def loadAllSequences(self):
        path =  "tests/sequences/"
        self.allSequences = []
        for (dirpath, dirnames, filenames) in os.walk(path):
             for file in filenames:
                 if file.endswith(".seq"):
                     self.allSequences.append(file[:-4])
        return
    
    def updateAllSeqUI(self):
        self.allSeqList.clear()
        if(len(self.allSequences) > 0):
            self.allSeqList.setColumnCount(3)
            self.allSeqList.setRowCount(len(self.allSequences))
        else:
            self.allSeqList.setColumnCount(0)
            self.allSeqList.setRowCount(0)

        i = 0
        for file in self.allSequences:
            item = QTableWidgetItem(file)
            self.allSeqList.setItem(i,1,item)

            itemPeek = QTableWidgetItem("")
            icon_peek= QIcon('peek.png')
            itemPeek.setIcon(icon_peek)
            self.allSeqList.setItem(i,0,itemPeek)
            
            itemDelete = QTableWidgetItem("")
            icon_delete= QIcon('right.png')
            itemDelete.setIcon(icon_delete)
            self.allSeqList.setItem(i,2,itemDelete)
            i = i+1
            
        return
        return

    
    def createButtonPanel(self):
        buttons = QGroupBox()
        buttons.setMaximumWidth(350)
        layout = QGridLayout()
        
        save_button = QPushButton("Save")
        save_button.released.connect(self.saveClicked)

        new_button = QPushButton("Start New Sequence")
        new_button.released.connect(self.startNew)
        
        exit_button = QPushButton("Exit")
        exit_button.released.connect(self.close)
        layout.addWidget(save_button,0,0)
        layout.addWidget(new_button,0,1)
        layout.addWidget(exit_button,0,2)
        
        
        buttons.setLayout(layout)
        return buttons

    def createAllFileButtonPanel(self):
        buttons = QGroupBox()
        buttons.setMaximumWidth(150)
        layout = QGridLayout()

        refresh_button = QPushButton("Refresh List")
        refresh_button.released.connect(self.refreshAllSuitesAndSegs)
        layout.addWidget(refresh_button,0,0)
        buttons.setLayout(layout)

        return buttons
        
        
        
    def createOutputPanel(self):
        self.outputList = QTableWidget(0,4)
        self.outputList.verticalHeader().setVisible(False)
        self.outputList.horizontalHeader().setVisible(False)
        self.outputList.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.outputList.setColumnWidth(0,250)
        self.outputList.setColumnWidth(1,20)
        self.outputList.setColumnWidth(2,20)
        self.outputList.setColumnWidth(3,20)
        self.outputList.itemClicked.connect(self.cellClicked)
        self.outputList.setShowGrid(False)
        
        outputBoxLayout = QVBoxLayout()
        self.outputList.setMinimumWidth(350)
        self.outputList.setMaximumWidth(350)

        self.outputList.setLayout(outputBoxLayout)
        return self.outputList


    def createAllFilePanel(self):
        self.allFileList = QTableWidget(0,2)
        self.allFileList.verticalHeader().setVisible(False)
        self.allFileList.horizontalHeader().setVisible(False)

        self.allFileList.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.allFileList.setColumnWidth(0,200)
        self.allFileList.setColumnWidth(1,20)
        self.allFileList.itemClicked.connect(self.allFileClicked)

        self.allFileList.itemDoubleClicked.connect(self.allFileDoubleClicked)

        self.allFileList.setShowGrid(False)
        
        layout = QVBoxLayout()
        self.allFileList.setMinimumWidth(230)
        self.allFileList.setLayout(layout)
        return self.allFileList
        
        
    def addNewSeqToSuite(self,clickedIndex):
        row = clickedIndex.row()
        col = clickedIndex.column()

        if col == 2:
            seqName = self.allSeqList.item(row,1).text()
            self.testSuite.add_to_suite(seqName)
        else:
            #ignore
            return      
        self.updateOutputPanel()

    def addToSeqDoubleClicked(self,clickedIndex):
        row = clickedIndex.row()
        col = clickedIndex.column()

        if col == 1:
            seqName = self.allSeqList.item(row,1).text()
            self.testSuite.add_to_suite(seqName)
        else:
            return #igore
        self.updateOutputPanel()

                
    def updateOutputPanel(self):
        self.outputList.clear()
        if(len(self.testSuite.suite_sequence) > 0):
            self.outputList.setColumnCount(4)
            self.outputList.setRowCount(len(self.testSuite.suite_sequence))
        else:
            self.outputList.setRowCount(0)
            self.outputList.setRowCount(0)
        
        i = 0
        for seq in self.testSuite.suite_sequence:
            name = seq
            item = QTableWidgetItem(name)
            self.outputList.setItem(i,0,item)
            
            itemMoveUp = QTableWidgetItem("")
            icon_moveup = QIcon('up.png')
            itemMoveUp.setIcon(icon_moveup)
            self.outputList.setItem(i,1,itemMoveUp)

            itemMoveDown = QTableWidgetItem("")
            icon_movedown = QIcon('down.png')
            itemMoveDown.setIcon(icon_movedown)
            self.outputList.setItem(i,2,itemMoveDown)

            itemDelete = QTableWidgetItem("")
            icon_delete= QIcon('delete.png')
            itemDelete.setIcon(icon_delete)
            self.outputList.setItem(i,3,itemDelete)
            i=i+1
        #self.outputBoxLayout.addWidget(QPushButton(elf.testSeq[))
        #refresh the output panel
        return
                
    def cellClicked(self, clickedIndex):
        row = clickedIndex.row()
        
        if clickedIndex.column() == 0:
            return #do nothing, may be in future edit the parameters
        elif clickedIndex.column() == 1: #move up
            self.testSuite.move_item_up(row)
        elif clickedIndex.column() == 2: #move down
            self.testSuite.move_item_down(row)
        elif clickedIndex.column() == 3: #delete
            self.testSuite.delete_item(row)
        else:
            return
            
        #update table with latest data, since it changed
        self.updateOutputPanel()

    def saveClicked(self):
        self.testSuite.suite_name = self.nameInput.text()
        self.testSuite.suite_developer = self.developerInput.text()
        self.testSuite.suite_description = self.descriptionInput.text()
        err = self.testSuite.save()

        if(err[0] != PASS):
            easygui.msgbox(err[1])
        else:
            easygui.msgbox("Suite Saved Successfully")
            self.loadAllFiles()
            self.updateAllFileUI()
            self.startNew()

    def updateAllFileUI(self):
        self.allFileList.clear()
        if(len(self.allFiles) > 0):
            self.allFileList.setColumnCount(2)
            self.allFileList.setRowCount(len(self.allFiles))
        else:
            self.allFileList.setColumnCount(0)
            self.allFileList.setRowCount(0)

        i = 0
        for file in self.allFiles:
            item = QTableWidgetItem(file)
            self.allFileList.setItem(i,0,item)

            itemDelete = QTableWidgetItem("")
            icon_delete= QIcon('delete.png')
            itemDelete.setIcon(icon_delete)
            self.allFileList.setItem(i,1,itemDelete)
            
            i = i+1
            
        return
    
    def loadAllFiles(self):
        path =  "tests/suites/"
        self.allFiles = []
        for (dirpath, dirnames, filenames) in os.walk(path):
             for file in filenames:
                 if file.endswith(".sui"):
                     self.allFiles.append(file[:-4])
        return

    
    def allFileClicked(self, clickedIndex):
        row = clickedIndex.row()
        col = clickedIndex.column()
        path =  "tests/suites/"
        
        if col == 0: #do nothign with a single click
            return
        elif col == 1: #delete
            #delete File
            nameOnly = self.allFileList.item(row,0).text()
            name = path+nameOnly+".sui"
            try:
                os.remove(name)
                self.allFiles.pop(row)
                easygui.msgbox("Successfully Deleted " + nameOnly)
            except:
                easygui.msgbox("Error Deleting sequence " + nameOnly)
            self.updateAllFileUI()
        return

    def allFileDoubleClicked(self,clickedIndex):
        row = clickedIndex.row()
        col = clickedIndex.column()
        
        if col == 0: #load file
            nameOnly = self.allFileList.item(row,0).text()
            self.testSuite = TestSuite()
            self.testSuite.load(nameOnly)
            self.nameInput.setText(self.testSuite.suite_name)
            self.developerInput.setText(self.testSuite.suite_developer)
            self.descriptionInput.setText(self.testSuite.suite_description)
            self.updateOutputPanel()
            return
        elif col == 1: #delete
            # do nithign on a doubel click on delete
            return

    def refreshAllSuitesAndSegs(self):
        self.loadAllFiles()
        self.updateAllFileUI()

        self.loadAllSequences()
        self.updateAllSeqUI()
        
    def startNew(self):
        self.testSuite = TestSuite()
        self.nameInput.setText("")
        self.developerInput.setText("")
        self.descriptionInput.setText("")
        self.updateOutputPanel()
        return
        
if __name__ == '__main__':
    
    app = QApplication(sys.argv)
    ex = TestSuiteCreator()
    sys.exit(app.exec_())
