
#This file contains all the possible sequence in a test
#Each test is a function. This file does not contain function dependencies
#If a test needs an additional resource, it will have to be included
#This file also contains a dictionary of test function name and
#what the end user name of the function

#function call types
FUNCTION_RUN = "RUN"
FUNCTION_TEST_PARAMS = "TEST_PARAMS"

#ERROR CODES
PASS = 0
FAIL = 1
INVALID_ARGUMENT = 2
UNABLE_TO_OPEN_CAMERA = 3
INVALID_TEST = 4
INVALID_FUNC_CALL_TYPE = 5
ALREADY_EXISTS = 6
MASTER_FILE_UNEDITABLE = 7
CALIB_DATA_NOT_FOUND = 8
CALIB_DATA_CORRUPT = 9

# The first parameter should be either "RUN" or "TEST_PARAMS"
def turn_off_movement(call_type, *args):
    if len(args) > 0:
        return ( ( INVALID_ARGUMENT, "Too many arguments supplied. None Required."))
        
    if call_type == FUNCTION_TEST_PARAMS:
        return( (PASS, "No Errors" ) )

    #continue test run
    return

def turn_on_movement(call_type, *args):
    if len(args) > 0:
        return ( ( INVALID_ARGUMENT, "Too many arguments supplied. None Required."))
        
    if call_type == FUNCTION_TEST_PARAMS:
        return( (PASS, "No Errors" ) )

    #continue test run
    return

def turn_off_system(call_type, *args):
    if len(args) > 0:
        return ( ( INVALID_ARGUMENT, "Too many arguments supplied. None Required."))
        
    if call_type == FUNCTION_TEST_PARAMS:
        return( (PASS, "No Errors" ) )

    #continue test run
    return

def turn_on_system(call_type, *args):
    if len(args) > 0:
        return ( ( INVALID_ARGUMENT, "Too many arguments supplied. None Required."))
        
    if call_type == FUNCTION_TEST_PARAMS:
        return( (PASS, "No Errors" ) )

    #continue test run
    return

def wait(call_type, *args):
    if len(args) != 1:
        return ( ( INVALID_ARGUMENT, "Too many/few arguments supplied. 1 Required."))
    time_in_seconds = args[0]
    try:
        time_in_seconds = int(time_in_seconds)
    except:
        return ( ( INVALID_ARGUMENT, "The Time in seconds should be a Number"))
            
    if call_type == FUNCTION_TEST_PARAMS: #if call type was just a test, then return pass here else continue
        return( ( PASS, "No Errors" ) )

    #if this was an actual test run, we will continue down
    return

def make_sure_time_is(call_type, *args):
    if len(args) != 1:
        return ( ( INVALID_ARGUMENT, "Too many/few arguments supplied. 1 Required."))
    time_in_seconds = args[0]
    try:
        time_in_seconds = int(time_in_seconds)
    except:
        return ( ( INVALID_ARGUMENT, "The Time in seconds should be a Number"))
            
    if call_type == FUNCTION_TEST_PARAMS: #if call type was just a test, then return pass here else continue
        return( ( PASS, "No Errors" ) )

    #if this was an actual test run, we will continue down
    return

def drain_super_cap_to_volt(call_type,*args):
    if len(args) != 1:
        return ( ( INVALID_ARGUMENT, "Too many/few arguments supplied. 1 Required."))
    drain_to_volt = args[0]
    try:
        drain_to_volt = float(drain_to_volt)
    except:
        return ( ( INVALID_ARGUMENT, "Voltage should be a Number"))
            
    if call_type == FUNCTION_TEST_PARAMS: #if call type was just a test, then return pass here else continue
        return( ( PASS, "No Errors" ) )

    #if this was an actual test run, we will continue down
    return
    

def forget_correction_code(call_type, *args):
    if len(args) > 0:
        return ( ( INVALID_ARGUMENT, "Too many arguments supplied. None Required."))
        
    if call_type == FUNCTION_TEST_PARAMS:
        return( (PASS, "No Errors" ) )

    #continue test run
    return

def set_time_in_master(call_type,*args):
    if len(args) != 1:
        return ( ( INVALID_ARGUMENT, "Too many/few arguments supplied. 1 Required."))
    time_in_seconds = args[0]
    try:
        time_in_seconds = int(time_in_seconds)
    except:
        return ( ( INVALID_ARGUMENT, "The Time in seconds should be a Number"))
            
    if call_type == FUNCTION_TEST_PARAMS: #if call type was just a test, then return pass here else continue
        return( ( PASS, "No Errors" ) )

    #if this was an actual test run, we will continue down
    return

def set_correction_code_in_master(call_type,*args):
    if len(args) != 1:
        return ( ( INVALID_ARGUMENT, "Too many/few arguments supplied. 1 Required."))
    correction_code = args[0]
    try:
        correction_code = int(correction_code)
    except:
        return ( ( INVALID_ARGUMENT, "Correction Code should be a Number"))
            
    if call_type == FUNCTION_TEST_PARAMS: #if call type was just a test, then return pass here else continue
        return( ( PASS, "No Errors" ) )

    #if this was an actual test run, we will continue down
    return


#format
# "Name of Test": [ function_name, [list of arguments to function] ]
Tests = {
    "Turn Off Movement"                 :[ turn_off_movement,               [] ],
    "Turn On Movement"                  :[ turn_on_movement,                [] ],
    "Turn Off System"                   :[ turn_off_system,                 [] ],
    "Turn On System"                    :[ turn_on_system,                  [] ],
    "Wait"                              :[ wait,                            ["Time in Seconds"] ],
    "Make Sure Time is"                 :[ make_sure_time_is,               ["Time in Seconds"] ],
    "Drain Super Cap to Volt"           :[ drain_super_cap_to_volt,         ["Voltage to Drain To"] ],
    "Forget Correction Code"            :[ forget_correction_code,          [] ],
    "Set Time in Master"                :[ set_time_in_master,              ["Time in Seconds"] ],
    "Set Correction Code in Master"     :[ set_correction_code_in_master,   ["Correction Code"] ],
    }

listOfTests = []
for key in Tests:
    listOfTests.append(key)
